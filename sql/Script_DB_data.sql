/*
Navicat MySQL Data Transfer

Source Server         : Servidor MySQL
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : patrones

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-03-05 21:52:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) NOT NULL,
  `product_description` text NOT NULL,
  `units_in_stock` int(11) NOT NULL,
  `product_category_id` bigint(20) unsigned NOT NULL,
  `reward_points_credit` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `product_category_id` (`product_category_id`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`product_category_id`) REFERENCES `product_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', 'LECHE SABORIZADA – CHIQUI', 'La leche aún más irresistible, en sus 4 sabores.', '100', '1', '0');
INSERT INTO `product` VALUES ('2', 'LECHE CON CAFÉ 800ML', 'Calidad, sabor, conveniencia y practicidad en un mismo empaque  ', '50', '1', '0');
INSERT INTO `product` VALUES ('3', 'LECHE NATURAL LARGA VIDA', 'Ultrapasteurizada enriquecida con Vitaminas A, D3 y E, en sus 2 presentaciones.', '110', '1', '0');
INSERT INTO `product` VALUES ('4', 'LECHE FRESCA', 'Tu leche de siempre ', '80', '1', '0');
INSERT INTO `product` VALUES ('5', 'LECHE ENTERA', 'Más cremosa que nunca, en sus 2 presentaciones', '10', '1', '0');
INSERT INTO `product` VALUES ('7', 'YOGURT PREBIÓTICO “SBELT”', 'Es un producto lácteo, acidificado por acción biológica de bacterias lácticas específicas, elaborado con leche descremada seleccionada y controlada, azúcar, pulpa molida (de pasa) de ciruela, leche descremada en polvo, fibra soluble (1200), esencia de ciruela, estabilizantes (440ii y 1404), es un producto enriquecido con vitaminas A y D3', '5', '2', '0');
INSERT INTO `product` VALUES ('8', 'YOGURT TIPO GRIEGO “GRECO”', 'Un irresistible yogur cremoso en sus 3 variedades. ', '15', '2', '0');
INSERT INTO `product` VALUES ('9', 'ALIMENTO LACTEO PURA VIDA ENRIQUECIDA', 'Pura Vida, pura salud ', '20', '2', '0');
INSERT INTO `product` VALUES ('10', 'ALIMENTO LÁCTEO EVAPORADO', 'Deliciosa y Nutritiva', '15', '2', '0');
INSERT INTO `product` VALUES ('11', 'YOGURT BEBIBLE PIL 2 L.', 'El más clásico de los yogures, en sus 2 sabores ', '5', '2', '0');
INSERT INTO `product` VALUES ('12', '“GOJU” NÉCTAR DE FRUTA (ENVASE TETRA PAK®) 1 L.', 'Especial para los jóvenes de hoy, en sus 4 sabores.  ', '10', '3', '0');
INSERT INTO `product` VALUES ('13', '“GOJU” NÉCTAR DE FRUTA (ENVASE TETRA PAK®) 200 ML.', 'Especial para los jóvenes de hoy, en sus 4 sabores. ', '5', '3', '0');
INSERT INTO `product` VALUES ('14', 'JUGUITO PIL', 'El jugo preferido de los chicos, en sus 4 sabores', '200', '3', '0');
INSERT INTO `product` VALUES ('15', 'NÉCTAR PIL', 'En práctica lata para llevar y tomar ', '7', '3', '0');
INSERT INTO `product` VALUES ('16', 'NÉCTAR DE FRUTAS PIL 200 ML', 'El néctar que mejor encaja contigo, en sus 4 sabores  ', '13', '3', '0');
INSERT INTO `product` VALUES ('17', 'PASTAS “PIL”', 'FIDEOS (Vitaminas y fortificadas), en sus 6 variedades y presentaciones.  ', '100', '4', '0');
INSERT INTO `product` VALUES ('18', 'ALIMENTO BEBIBLE DE SOYA ‘SOY’ 946 ML', 'Tu buen hábito de cada día, en sus 5 sabores ', '10', '4', '0');
INSERT INTO `product` VALUES ('19', 'CAFÉ LIOFILIZADO PREMIUM MÓNACO', 'Lo premium tiene su premio', '1', '4', '0');
INSERT INTO `product` VALUES ('20', 'MARGARINA REYNA', 'Reyna sólo hay una, en sus 6 presentaciones', '21', '4', '0');
INSERT INTO `product` VALUES ('21', 'MERMELADA DE FRUTA 720 G', 'El dulce sabor de casa, en sus 2 sabores', '6', '4', '0');

-- ----------------------------
-- Table structure for product_category
-- ----------------------------
DROP TABLE IF EXISTS `product_category`;
CREATE TABLE `product_category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) NOT NULL,
  `max_reward_points_encash` int(11) NOT NULL,
  `parent_category_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `parent_category_id` (`parent_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_category
-- ----------------------------
INSERT INTO `product_category` VALUES ('1', 'LACTEOS', '0', '0');
INSERT INTO `product_category` VALUES ('2', 'DERIVADOS LACTEOS', '0', '0');
INSERT INTO `product_category` VALUES ('3', 'BEBIDAS', '0', '0');
INSERT INTO `product_category` VALUES ('4', 'OTROS PRODUCTOS', '0', '0');

-- ----------------------------
-- Table structure for product_category_discount
-- ----------------------------
DROP TABLE IF EXISTS `product_category_discount`;
CREATE TABLE `product_category_discount` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_category_id` bigint(20) unsigned NOT NULL,
  `discount_value` int(11) NOT NULL,
  `discount_unit` varchar(20) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `valid_until` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `coupon_code` varchar(10) NOT NULL,
  `minimun_order_value` int(11) NOT NULL,
  `maximun_discount_amount` int(11) NOT NULL,
  `is_redeem_allowed` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `product_category_id` (`product_category_id`),
  CONSTRAINT `product_category_discount_ibfk_1` FOREIGN KEY (`product_category_id`) REFERENCES `product_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_category_discount
-- ----------------------------
INSERT INTO `product_category_discount` VALUES ('1', '1', '25', '%', '2018-03-04 03:30:23', '2018-03-31 23:55:46', 'EXAMEN', '1', '500', '1');
INSERT INTO `product_category_discount` VALUES ('2', '2', '33', '%', '2018-03-04 03:30:25', '2018-03-31 23:56:09', 'EXAMEN', '1', '200', '1');
INSERT INTO `product_category_discount` VALUES ('3', '3', '85', '%', '2018-03-04 03:30:24', '2018-03-31 23:56:44', 'EXAMEN', '1', '300', '1');
INSERT INTO `product_category_discount` VALUES ('4', '4', '99', '%', '2018-03-04 03:30:26', '2018-03-31 23:57:14', 'EXAMEN', '1', '400', '1');

-- ----------------------------
-- Table structure for product_discount
-- ----------------------------
DROP TABLE IF EXISTS `product_discount`;
CREATE TABLE `product_discount` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL,
  `discount_value` int(11) DEFAULT NULL,
  `discount_unit` varchar(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `valid_until` timestamp NULL DEFAULT NULL,
  `coupon_code` varchar(10) DEFAULT NULL,
  `minimum_order_value` int(11) DEFAULT NULL,
  `maximum_discount_amount` int(11) DEFAULT NULL,
  `id_redeem_allowed` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `product_discount_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_discount
-- ----------------------------
INSERT INTO `product_discount` VALUES ('1', '1', '50', '%', '2018-03-01 23:21:31', '2018-03-31 23:23:23', 'EXAMEN', '1', '100', '1');
INSERT INTO `product_discount` VALUES ('2', '2', '50', '%', '2018-03-01 23:24:33', '2018-03-31 23:24:36', 'EXAMEN', '2', '49', '1');
INSERT INTO `product_discount` VALUES ('3', '3', '25', '%', '2018-03-01 23:29:36', '2018-03-31 23:29:40', 'EXAMEN', '1', '56', '1');
INSERT INTO `product_discount` VALUES ('4', '4', '25', '%', '2018-03-01 23:30:39', '2018-03-31 23:30:43', 'EXAMEN', '3', '30', '1');
INSERT INTO `product_discount` VALUES ('5', '5', '10', '%', '2018-03-01 23:31:44', '2018-03-31 23:31:54', 'EXAMEN', '1', '27', '1');
INSERT INTO `product_discount` VALUES ('6', '7', '5', '%', '2018-03-01 23:38:47', '2018-03-31 23:38:51', 'EXAMEN', '1', '6', '1');
INSERT INTO `product_discount` VALUES ('7', '8', '7', '%', '2018-03-01 23:40:01', '2018-03-31 23:40:06', 'EXAMEN', '2', '60', '1');
INSERT INTO `product_discount` VALUES ('8', '9', '25', '%', '2018-03-01 23:40:56', '2018-03-31 23:41:00', 'EXAMEN', '1', '33', '1');
INSERT INTO `product_discount` VALUES ('9', '10', '45', '%', '2018-03-01 23:41:57', '2018-03-31 23:42:01', 'EXAMEN', '3', '38', '1');
INSERT INTO `product_discount` VALUES ('10', '11', '10', '%', '2018-03-01 23:42:53', '2018-03-31 23:42:56', 'EXAMEN', '1', '15', '1');
INSERT INTO `product_discount` VALUES ('11', '12', '15', '%', '2018-03-01 23:46:52', '2018-03-31 23:46:55', 'EXAMEN', '1', '20', '1');
INSERT INTO `product_discount` VALUES ('12', '13', '10', '%', '2018-03-01 23:47:51', '2018-03-31 23:47:55', 'EXAMEN', '1', '30', '1');
INSERT INTO `product_discount` VALUES ('13', '14', '10', '%', '2018-03-01 23:48:40', '2018-03-31 23:48:43', 'EXAMEN', '5', '100', '1');
INSERT INTO `product_discount` VALUES ('14', '15', '30', '%', '2018-03-01 23:49:40', '2018-03-31 23:49:43', 'EXAMEN', '1', '110', '1');
INSERT INTO `product_discount` VALUES ('15', '16', '7', '%', '2018-03-01 23:50:21', '2018-03-31 23:50:27', 'EXAMEN', '2', '75', '1');
INSERT INTO `product_discount` VALUES ('16', '17', '10', '%', '2018-03-01 23:51:13', '2018-03-31 23:51:17', 'EXAMEN', '1', '88', '1');
INSERT INTO `product_discount` VALUES ('17', '18', '10', '%', '2018-03-01 23:51:46', '2018-03-31 23:51:49', 'EXAMEN', '5', '200', '1');
INSERT INTO `product_discount` VALUES ('18', '19', '15', '%', '2018-03-01 23:52:24', '2018-03-31 23:52:27', 'EXAMEN', '1', '75', '1');
INSERT INTO `product_discount` VALUES ('19', '20', '20', '%', '2018-03-01 23:53:19', '2018-03-31 23:53:21', 'EXAMEN', '2', '99', '1');
INSERT INTO `product_discount` VALUES ('20', '21', '30', '%', '2018-03-01 23:53:54', '2018-03-31 23:53:57', 'EXAMEN', '1', '100', '1');
INSERT INTO `product_discount` VALUES ('21', '1', '50', '%', '2018-03-01 09:20:23', '2018-03-05 09:20:26', 'PASADO', '1', '1000', '0');

-- ----------------------------
-- Table structure for product_pricing
-- ----------------------------
DROP TABLE IF EXISTS `product_pricing`;
CREATE TABLE `product_pricing` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL,
  `base_price` int(11) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `date_expiry` timestamp NULL DEFAULT NULL,
  `in_active` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `product_id_2` (`product_id`),
  KEY `product_id_3` (`product_id`),
  CONSTRAINT `product_pricing_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_pricing
-- ----------------------------
INSERT INTO `product_pricing` VALUES ('1', '1', '2', '2018-02-01 07:36:34', '2018-02-28 07:36:42', '1');
INSERT INTO `product_pricing` VALUES ('2', '2', '7', '2018-02-08 07:50:58', '2018-03-02 07:51:02', '1');
INSERT INTO `product_pricing` VALUES ('3', '3', '8', '2018-01-30 07:51:17', '2018-02-22 07:51:21', '1');
INSERT INTO `product_pricing` VALUES ('4', '4', '10', '2018-01-30 07:51:36', '2018-02-12 07:51:40', '1');
INSERT INTO `product_pricing` VALUES ('5', '5', '9', '2018-02-01 07:51:57', '2018-02-10 07:52:01', '1');
INSERT INTO `product_pricing` VALUES ('6', '7', '2', '2018-02-07 07:52:25', '2018-03-07 07:52:29', '1');
INSERT INTO `product_pricing` VALUES ('7', '8', '15', '2018-02-06 07:52:41', '2018-03-06 07:52:44', '1');
INSERT INTO `product_pricing` VALUES ('8', '9', '11', '2018-02-06 07:52:58', '2018-03-10 07:53:02', '1');
INSERT INTO `product_pricing` VALUES ('9', '10', '19', '2018-01-04 07:53:31', '2018-04-07 07:53:39', '1');
INSERT INTO `product_pricing` VALUES ('10', '11', '15', '2018-02-01 07:54:04', '2018-02-28 07:54:08', '1');
INSERT INTO `product_pricing` VALUES ('11', '12', '20', '2018-01-29 07:54:38', '2018-03-11 07:54:41', '1');
INSERT INTO `product_pricing` VALUES ('12', '13', '21', '2018-02-06 07:54:53', '2018-03-11 07:54:56', '1');
INSERT INTO `product_pricing` VALUES ('13', '14', '2', '2018-02-07 07:55:17', '2018-03-07 07:55:19', '1');
INSERT INTO `product_pricing` VALUES ('14', '15', '17', '2018-01-03 07:55:31', '2018-03-31 07:55:36', '1');
INSERT INTO `product_pricing` VALUES ('15', '16', '25', '2018-01-31 07:56:07', '2018-03-02 07:56:10', '1');
INSERT INTO `product_pricing` VALUES ('16', '17', '10', '2018-02-01 07:56:49', '2018-04-18 07:56:53', '1');
INSERT INTO `product_pricing` VALUES ('17', '18', '8', '2018-02-07 07:57:13', '2018-03-08 07:57:16', '1');
INSERT INTO `product_pricing` VALUES ('18', '19', '75', '2018-01-01 07:57:40', '2018-11-30 07:57:45', '1');
INSERT INTO `product_pricing` VALUES ('19', '20', '10', '2018-02-06 07:58:09', '2018-02-10 07:58:12', '1');
INSERT INTO `product_pricing` VALUES ('20', '21', '18', '2018-02-06 07:58:29', '2018-03-10 07:58:32', '1');
