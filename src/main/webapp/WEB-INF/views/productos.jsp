<%@page session="false"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!doctype html>
<html lang="en">

  <head>
<!--     <meta charset="utf-8"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
<!--     <link rel="icon" href="favicon.ico"> -->
	<spring:url value="/resources/images/bootstrap/favicon.ico"
		var="faviconIco" />
	<link href="${faviconIco}" rel="icon" />

    <title>Trabajo Final - Patrones de Diseño</title>

    <!-- Bootstrap core CSS -->
<!--     <link href="css/bootstrap.min.css" rel="stylesheet"> -->
	<spring:url value="/resources/css/bootstrap/bootstrap.min.css"
		var="bootstrapCss" />
	<link href="${bootstrapCss}" rel="stylesheet" />

    <!-- Custom styles for this template -->
<!--     <link href="pricing.css" rel="stylesheet"> -->
	<spring:url value="/resources/css/pricing.css"
		var="pricingCss" />
	<link href="${pricingCss}" rel="stylesheet" />
    
  </head>

  <body>

    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal">Trabajo Final</h5>
      <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="#">Producto</a>
        <a class="p-2 text-dark" href="#">Precio del producto</a>
        <a class="p-2 text-dark" href="#">Categoria de los productos</a>
        <a class="p-2 text-dark" href="#">Descuento por Producto</a>
        <a class="p-2 text-dark" href="#">Descuento por Categoria</a>
      </nav>
      <a class="btn btn-outline-primary" href="#">Sign up</a>
    </div>

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">${categoria.categoryName}</h1>
      <p class="lead">¿QUÉ TE GUSTARÍA PROBAR HOY?</p>
    </div>

    <div class="container">
    
      <div class="card-deck mb-3 text-center">
		<c:forEach items="${listaProductPrice}" var="array" varStatus="status">
	        <div class="card mb-4 box-shadow">
	          <div class="card-header">
	            <h4 class="my-0 font-weight-normal">${array.product.productName}/${array.product.unitsInStock}</h4>
	          </div>
	          <div class="card-body">

	            <h2 class="card-title pricing-card-title">${array.productPricing.basePrice} Bs</h2>
<!--	            <ul class="list-unstyled mt-3 mb-4">
	              <li>10 users included</li>
	              <li>2 GB of storage</li>
	              <li>Email support</li>
	              <li>Help center access</li>
	            </ul>
	            -->
	            
				
				<!-- Material form register -->
				<form name="form${array.product.id}" action="pagar_producto" target="_blank">				    
				
				    <!-- Material input cantidad -->
				    <div class="md-form">
				        <i class="fa fa-user prefix grey-text"></i>
				        <input type="text" name="materialFormRegisterCantidad" class="form-control">
				        <label for="materialFormRegisterCantidad${array.product.id}">Cantidad</label>
				    </div>
				
				    <!-- Material input cupon -->
				    <div class="md-form">
				        <i class="fa fa-envelope prefix grey-text"></i>
				        <input type="text" name="materialFormRegisterCupon" class="form-control">
				        <label for="materialFormRegisterCupon${array.product.id}">Cupón</label>
				    </div>
	
				    <div class="text-center mt-4">
				    	<input type=text name="idProducto" id="idProducto" value=${array.product.id}>
			            <button type="submit" class="btn btn-lg btn-block btn-primary">Pagar</button>
				    </div>
				</form>
				<!-- Material form register -->                      
				<!-- 				                      
	            <button type="button" class="btn btn-lg btn-block btn-primary">Añadir</button>
	             -->
	          </div>
	        </div>
		</c:forEach>
      </div>
  
      <footer class="pt-4 my-md-5 pt-md-5 border-top">
        <div class="row">
          <div class="col-12 col-md">
            <img class="mb-2" src="bootstrap-solid.svg" alt="" width="24" height="24">
            <small class="d-block mb-3 text-muted">&copy; 2017-${year}</small>
          </div>
          <div class="col-6 col-md">
            <h5>Integrantes</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Navarro Arias Juan Dirceu</a></li>
              <li><a class="text-muted" href="#">Navarro Arias Luis Fernando Numa</a></li>              
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Modulo</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Patrones de Diseño</a></li>              
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Maestria</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Desarrollo Web</a></li>              
            </ul>
          </div>
        </div>
      </footer>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../../../assets/js/vendor/popper.min.js"></script>
    <script src="../../../../dist/js/bootstrap.min.js"></script>
    <script src="../../../../assets/js/vendor/holder.min.js"></script>
    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    </script>
  </body>
</html>
