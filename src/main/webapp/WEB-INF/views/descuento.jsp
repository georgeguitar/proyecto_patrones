<%@page session="false"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!doctype html>
<html lang="en">

  <head>
<!--     <meta charset="utf-8"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
	<spring:url value="/resources/images/bootstrap/favicon.ico"
		var="faviconIco" />
	<link href="${faviconIco}" rel="icon" />

    <title>Trabajo Final - Patrones de Diseño</title>

    <!-- Bootstrap core CSS -->
	<spring:url value="/resources/css/bootstrap/bootstrap.min.css"
		var="bootstrapCss" />
	<link href="${bootstrapCss}" rel="stylesheet" />

    <!-- Custom styles for this template -->
	<spring:url value="/resources/css/pricing.css"
		var="pricingCss" />
	<link href="${pricingCss}" rel="stylesheet" />
    
  </head>

  <body>
  
<form:form action="productos"  method="POST" class="form-horizontal" id="main_form" target="_blank">


    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
      <h5 class="my-0 mr-md-auto font-weight-normal">Trabajo Final</h5>
      <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="#">Producto</a>
        <a class="p-2 text-dark" href="#">Precio del producto</a>
        <a class="p-2 text-dark" href="#">Categoria de los productos</a>
        <a class="p-2 text-dark" href="#">Descuento por Producto</a>
        <a class="p-2 text-dark" href="#">Descuento por Categoria</a>
      </nav>
      <a class="btn btn-outline-primary" href="#">Sign up</a>
    </div>

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">${nombreProducto}</h1>
      <p class="lead">TOTAL DESCUENTO POR PRODUCTO: ${descuentoTotalProducto}</p>
      <p class="lead">TOTAL DESCUENTO POR CATEGORIA: ${descuentoTotalCategoria}</p>
      <p class="lead">A pedido: ${cantidad} ${nombreCantidad}, precio: ${precioCategoria}</p>
      <p class="lead">TOTAL A PAGAR: ${aPagar}</p>
      <p class="lead">${mensajeRetorno}</p>
    </div>

    <div class="container">
      <div class="card-deck mb-3 text-center">



      </div>

      <footer class="pt-4 my-md-5 pt-md-5 border-top">
        <div class="row">
          <div class="col-12 col-md">
            <img class="mb-2" src="bootstrap-solid.svg" alt="" width="24" height="24">
            <small class="d-block mb-3 text-muted">&copy; 2017-2018</small>
          </div>
          <div class="col-6 col-md">
            <h5>Integrantes</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Navarro Arias Juan Dirceu</a></li>
              <li><a class="text-muted" href="#">Navarro Arias Luis Fernando Numa</a></li>              
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Modulo</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Patrones de Diseño</a></li>              
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Maestria</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Desarrollo Web</a></li>              
            </ul>
          </div>
        </div>
      </footer>
    </div>
    


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../../../assets/js/vendor/popper.min.js"></script>
    <script src="../../../../dist/js/bootstrap.min.js"></script>
    <script src="../../../../assets/js/vendor/holder.min.js"></script>
    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    </script>
    
</form:form>    
  </body>
</html>
