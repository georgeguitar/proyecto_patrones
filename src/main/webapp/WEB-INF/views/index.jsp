<%@page session="false"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!doctype html>
<html lang="en">

  <head>
<!--     <meta charset="utf-8"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    
<!--     <link rel="icon" href="/resources/images/bootstrap/favicon.ico"> -->
	<spring:url value="/resources/images/bootstrap/favicon.ico"
		var="faviconIco" />
	<link href="${faviconIco}" rel="icon" />
	
	
    <title>Trabajo Final - Patrones de Diseño</title>

    <!-- Bootstrap core CSS -->
<!--     <link href="css/bootstrap.min.css" rel="stylesheet"> -->
	<spring:url value="/resources/css/bootstrap/bootstrap.min.css"
		var="bootstrapCss" />
	<link href="${bootstrapCss}" rel="stylesheet" />

    <!-- Custom styles for this template -->
<!--     <link href="signin.css" rel="stylesheet"> -->
	<spring:url value="/resources/css/signin.css"
		var="signinCss" />
	<link href="${signinCss}" rel="stylesheet" />
  </head>


  <body class="text-center">
    <form class="form-signin" action="menu" method="post">
    
<!--       <img class="mb-4" src="bootstrap-solid.svg" alt="" width="72" height="72"> -->
		<spring:url value="/resources/images/bootstrap/bootstrap-solid.svg"
			var="bootstrapSolid" />
      <img class="mb-4" src="${bootstrapSolid}" alt="" width="72" height="72">
      
      <h1 class="h3 mb-3 font-weight-normal">Por favor, registrese</h1>
      <label for="inputEmail" class="sr-only">Dirección de correo</label>
      <input type="email" id="inputEmail" class="form-control" value="usuario@gmail.com" placeholder="Dirección de correo" required autofocus>
      <label for="inputPassword" class="sr-only">Contraseña</label>
      <input type="password" id="inputPassword" class="form-control" value="password" placeholder="Contraseña" required>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Recuerdeme
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Registrarse</button>
      <p class="mt-5 mb-3 text-muted">&copy; ${year} </p>
    </form>
  </body>
</html>
