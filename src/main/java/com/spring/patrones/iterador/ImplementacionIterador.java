package com.spring.patrones.iterador;

import java.util.Iterator;
import java.util.List;

import com.spring.model.ProductPrice;

class ImplementacionIterador implements Iterator<ProductPrice> {
	int indiceActual = 0;
	List<ProductPrice> elementos;
	
	public ImplementacionIterador(List<ProductPrice> _elementos) {
		elementos = _elementos;
	}

	@Override
	public boolean hasNext() {
		if (indiceActual >= elementos.size()) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public ProductPrice next() {
		return elementos.get(indiceActual++);
	}

	@Override
	public void remove() {
		elementos.remove(--indiceActual);
	}
}
