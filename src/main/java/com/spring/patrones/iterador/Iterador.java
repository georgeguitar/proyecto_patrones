package com.spring.patrones.iterador;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.spring.model.ProductPrice;

public class Iterador {
	List<ProductPrice> elementos;

	public Iterador() {
		elementos = new ArrayList<ProductPrice>();
	}

	public Iterator<ProductPrice> getIterador() {
		return new ImplementacionIterador(elementos);
	}
	
	public void addElemento(ProductPrice ProductPrice) {
		elementos.add(ProductPrice);
	}
}