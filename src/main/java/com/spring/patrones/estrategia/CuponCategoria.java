package com.spring.patrones.estrategia;

public class CuponCategoria extends Cupon {
	public Float aplicarDescuento() {
		return algoritmoDescuento.calcular();
	}
}
