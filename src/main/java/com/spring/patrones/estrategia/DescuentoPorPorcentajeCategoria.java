package com.spring.patrones.estrategia;

import com.spring.model.CategoryPrice;

public class DescuentoPorPorcentajeCategoria implements AlgoritmoDescuento {
	private CategoryPrice categoryPrice;
	private Integer cantidad;

	public DescuentoPorPorcentajeCategoria(CategoryPrice _categoryPrice, Integer _cantidad) {
		this.categoryPrice = _categoryPrice;
		this.cantidad = _cantidad;
	}
	
	@Override
	public Float calcular(){
		Float descuentoTotal = 0.0F;
		Integer discountValue = this.categoryPrice.getProductCategoryDiscount().getDiscountValue(); //Se supone es que en porcentaje
		String discountUnit = this.categoryPrice.getProductCategoryDiscount().getDiscountUnit();

		if ("%".equals(discountUnit)) {
			descuentoTotal = Float.valueOf(this.cantidad) * discountValue / 100;
			System.out.println("calculando descuento por porcentaje!!");
		}		
		return descuentoTotal;
	}
}