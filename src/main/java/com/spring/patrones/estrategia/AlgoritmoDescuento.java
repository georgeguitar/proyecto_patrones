package com.spring.patrones.estrategia;

public interface AlgoritmoDescuento{
	public Float calcular();
}