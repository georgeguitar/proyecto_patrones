package com.spring.patrones.estrategia;

public class CuponProducto extends Cupon {

	public Float aplicarDescuento() {
		return algoritmoDescuento.calcular();
	}
}
