package com.spring.patrones.estrategia;

import com.spring.model.ProductDiscount;

public class DescuentoPorPorcentajeProduct implements AlgoritmoDescuento {
	private ProductDiscount productDiscount;
	private Integer cantidad;

	public DescuentoPorPorcentajeProduct(ProductDiscount _productDiscount, Integer _cantidad) {
		productDiscount = _productDiscount;
		cantidad = _cantidad;
	}
	
	@Override
	public Float calcular(){
		Float descuentoTotal = 0.0F;
		Integer discountValue = this.productDiscount.getDiscountValue(); //Se supone es que en porcentaje
		String discountUnit = this.productDiscount.getDiscountUnit();
		
		if ("%".equals(discountUnit)) {
			descuentoTotal = Float.valueOf(this.cantidad) * discountValue / 100;
			
			System.out.println("calculando descuento por porcentaje!!");
		}
		 
		return descuentoTotal;
	}
}