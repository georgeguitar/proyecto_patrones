package com.spring.patrones.estrategia;

public abstract class Cupon {
	protected AlgoritmoDescuento algoritmoDescuento;
	
	public void setAlgoritmoDescuento(AlgoritmoDescuento a){
		this.algoritmoDescuento = a;
	}
}
