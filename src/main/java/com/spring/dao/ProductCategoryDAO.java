/**
 * 
 */
package com.spring.dao;

import java.util.List;

import com.spring.model.ProductCategory;

public interface ProductCategoryDAO {
	public List<ProductCategory> getListaProductosCategoria();
	public ProductCategory getCategoria(String idCategoria);
}
