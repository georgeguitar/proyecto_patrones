/**
 * 
 */
package com.spring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.spring.model.ProductCategory;


public class ProductCategoryDAOImpl implements ProductCategoryDAO {
	
	private JdbcTemplate jdbcTemplate;

	public ProductCategoryDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<ProductCategory> getListaProductosCategoria() {
		String sql = "SELECT * FROM product_category";
		List<ProductCategory> listaProductoCategoria = this.jdbcTemplate.query(sql, new RowMapper<ProductCategory>() {

			@Override
			public ProductCategory mapRow(ResultSet rs, int rowNum) throws SQLException {
				ProductCategory productCategory = new ProductCategory();
	
				productCategory.setId(rs.getLong("id"));
				productCategory.setCategoryName(rs.getString("category_name"));
				productCategory.setMaxRewardPointsEncash(rs.getInt("max_reward_points_encash"));
				productCategory.setParentCategoryID(rs.getLong("parent_category_id"));
				
				return productCategory;
			}
		});
		
		return listaProductoCategoria;
	}
	
	@Override
	public ProductCategory getCategoria(String idCategoria) {
		String sql = "SELECT * FROM product_category WHERE id = " + idCategoria;
		ProductCategory categoria = this.jdbcTemplate.query(sql, new ResultSetExtractor<ProductCategory>() {

			@Override
			public ProductCategory extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					ProductCategory productCategory = new ProductCategory();
					
					productCategory.setId(rs.getLong("id"));
					productCategory.setCategoryName(rs.getString("category_name"));
					productCategory.setMaxRewardPointsEncash(rs.getInt("max_reward_points_encash"));
					productCategory.setParentCategoryID(rs.getLong("parent_category_id"));
					
					return productCategory;
				}
				
				return null;
			}
			
		});
		
		return categoria;
	}
}
