/**
 * 
 */
package com.spring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.spring.model.Product;
import com.spring.model.ProductDiscount;


public class ProductDAOImpl implements ProductDAO {
	
	private JdbcTemplate jdbcTemplate;

	public ProductDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Product> getListaProductos(String idCategoria) {
		String sql = "SELECT * FROM product WHERE product_category_id = " + idCategoria;
		List<Product> listaProductos = this.jdbcTemplate.query(sql, new RowMapper<Product>() {

			@Override
			public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
				Product product = new Product();
	
				product.setId(rs.getLong("id"));
				product.setProductName(rs.getString("product_name"));
				product.setProductDescription(rs.getString("product_description"));
				product.setUnitsInStock(rs.getInt("units_in_stock"));
				product.setProductCategoryID(rs.getLong("product_category_id"));
				product.setRewardPointsCredit(rs.getInt("reward_points_credit"));
				
				return product;
			}
		});
		
		return listaProductos;
	}
	
	@Override
	public List<Product> getProductosCategoria(String idCategoria) {
		String sql = "SELECT * FROM product WHERE product_category_id = " + idCategoria;
		List<Product> listaProductos = this.jdbcTemplate.query(sql, new RowMapper<Product>() {

			@Override
			public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
				Product product = new Product();
	
				product.setId(rs.getLong("id"));
				product.setProductName(rs.getString("product_name"));
				product.setProductDescription(rs.getString("product_description"));
				product.setUnitsInStock(rs.getInt("units_in_stock"));
				product.setProductCategoryID(rs.getLong("product_category_id"));
				product.setRewardPointsCredit(rs.getInt("reward_points_credit"));
				
				return product;
			}
		});
		
		return listaProductos;
	}
	
	@Override
	public Product getProducto(String idProducto) {
		String sql = "SELECT * FROM product WHERE id = " + idProducto;
		//ProductDiscount productDiscount = this.jdbcTemplate.query(sql, new ResultSetExtractor<ProductDiscount>() {
		Product product = this.jdbcTemplate.query(sql, new ResultSetExtractor<Product>() {
				
			@Override
			public Product extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					Product product = new Product();
					
					product.setId(rs.getLong("id"));
					product.setProductName(rs.getString("product_name"));
					product.setProductDescription(rs.getString("product_description"));
					product.setUnitsInStock(rs.getInt("units_in_stock"));
					product.setProductCategoryID(rs.getLong("product_category_id"));
					product.setRewardPointsCredit(rs.getInt("reward_points_credit"));
										
					return product;
				}
				
				return null;
			}

		});
		return product;
	}
}
