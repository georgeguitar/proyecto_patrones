package com.spring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.spring.model.CategoryPrice;
import com.spring.model.Product;
import com.spring.model.ProductCategory;
import com.spring.model.ProductCategoryDiscount;
import com.spring.model.ProductPricing;

public class ProductCategoryDiscountDAOImpl implements ProductCategoryDiscountDAO {
	private JdbcTemplate jdbcTemplate;

	public ProductCategoryDiscountDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public CategoryPrice getProductCategoryDiscount(String productID, String couponCode) {
		String sql = "SELECT " + 
				"product.id AS p_id, " + 
				"product.product_name AS p_product_name, " + 
				"product.product_description AS p_product_description, " + 
				"product.units_in_stock AS p_units_in_stock, " + 
				"product.product_category_id AS p_product_category_id, " + 
				"product.reward_points_credit AS p_reward_points_credit, " + 
				"product_category.id AS pc_id, " + 
				"product_category.category_name AS pc_category_name, " + 
				"product_category.max_reward_points_encash AS pc_max_reward_points_encash, " + 
				"product_category.parent_category_id AS pc_parent_category_id, " + 
				"product_category_discount.id AS pcd_id, " + 
				"product_category_discount.product_category_id AS pcd_product_category_id, " + 
				"product_category_discount.discount_value AS pcd_discount_value, " + 
				"product_category_discount.discount_unit AS pcd_discount_unit, " + 
				"product_category_discount.date_created AS pcd_date_created, " + 
				"product_category_discount.valid_until AS pcd_valid_until, " + 
				"product_category_discount.coupon_code AS pcd_coupon_code, " + 
				"product_category_discount.minimun_order_value AS pcd_minimun_order_value, " + 
				"product_category_discount.maximun_discount_amount AS pcd_maximun_discount_amount, " + 
				"product_category_discount.is_redeem_allowed AS pcd_is_redeem_allowed, " + 
				"product_pricing.id As pp_id, " + 
				"product_pricing.product_id AS pp_product_id, " + 
				"product_pricing.base_price AS pp_base_price, " + 
				"product_pricing.date_created AS pp_date_created, " + 
				"product_pricing.date_expiry AS pp_date_expiry, " + 
				"product_pricing.in_active AS pp_in_active " + 
				"FROM " + 
				"product " + 
				"INNER JOIN product_category ON product.product_category_id = product_category.id " + 
				"INNER JOIN product_category_discount ON product_category_discount.product_category_id = product_category.id " + 
				"INNER JOIN product_pricing ON product_pricing.product_id = product.id " +				 
				"WHERE product.id = " + productID + " AND product_category_discount.coupon_code = '" + couponCode + "'"; 				

		CategoryPrice categoryPrice = this.jdbcTemplate.query(sql, new ResultSetExtractor<CategoryPrice>() {

			@Override
			public CategoryPrice extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {			
					
					CategoryPrice categoryPrice = new CategoryPrice();
					
					Product product = new Product();
					
					product.setId(rs.getLong("p_id"));
					product.setProductName(rs.getString("p_product_name"));
					product.setProductDescription(rs.getString("p_product_description"));
					product.setUnitsInStock(rs.getInt("p_units_in_stock"));
					product.setProductCategoryID(rs.getLong("p_product_category_id"));
					product.setRewardPointsCredit(rs.getInt("p_reward_points_credit"));
					
					ProductCategory productCategory = new ProductCategory();
					productCategory.setId(rs.getLong("pc_id"));
					productCategory.setCategoryName(rs.getString("pc_category_name"));
					productCategory.setMaxRewardPointsEncash(rs.getInt("pc_max_reward_points_encash"));
					productCategory.setParentCategoryID(rs.getLong("pc_parent_category_id"));
										
					ProductCategoryDiscount productCategoryDiscount = new ProductCategoryDiscount();
					productCategoryDiscount.setId(rs.getLong("pcd_id"));
					productCategoryDiscount.setProductCategoryId(rs.getLong("pcd_product_category_id"));					
					productCategoryDiscount.setDiscountValue(rs.getInt("pcd_discount_value"));
					productCategoryDiscount.setDiscountUnit(rs.getString("pcd_discount_unit"));
					productCategoryDiscount.setDateCreated(rs.getDate("pcd_date_created"));
					productCategoryDiscount.setValidUntil(rs.getDate("pcd_valid_until"));
					productCategoryDiscount.setCouponCode(rs.getString("pcd_coupon_code"));
					productCategoryDiscount.setMinimumOrderValue(rs.getInt("pcd_minimun_order_value"));
					productCategoryDiscount.setMaximumDiscountAmount(rs.getInt("pcd_maximun_discount_amount"));
					productCategoryDiscount.setIdRedeemAllowed(rs.getString("pcd_is_redeem_allowed"));
					
					ProductPricing productPricing = new ProductPricing();
					productPricing.setId(rs.getLong("pp_id"));
					productPricing.setProductId(rs.getLong("pp_product_id"));
					productPricing.setBasePrice(rs.getInt("pp_base_price"));
					productPricing.setDateCreated(rs.getDate("pp_date_created"));
					productPricing.setDateExpiry(rs.getDate("pp_date_expiry"));
					productPricing.setInActive(rs.getString("pp_in_active"));
					
					categoryPrice.setProduct(product);
					categoryPrice.setProductCategory(productCategory);
					categoryPrice.setProductCategoryDiscount(productCategoryDiscount);
					categoryPrice.setProductPricing(productPricing);
					
					return categoryPrice;
				}
				
				return null;
			}
			
		});
		return categoryPrice;
	}
}
