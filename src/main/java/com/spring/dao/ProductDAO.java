/**
 * 
 */
package com.spring.dao;

import java.util.List;

import com.spring.model.Product;

/**
 * @author Juan Navarro
 *
 */
public interface ProductDAO {
	public List<Product> getListaProductos(String idCategoria);
	public List<Product> getProductosCategoria(String idCategoria);
	public Product getProducto(String idProducto);
}
