package com.spring.dao;

import java.sql.Date;

public interface SistemaGeneralDAO {
	public Date getDateServer();
	public String getYearServer();
}
