package com.spring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.spring.model.ProductPricing;

public class ProductPricingDAOImpl implements ProductPricingDAO {
	private JdbcTemplate jdbcTemplate;

	public ProductPricingDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public ProductPricing getProductPricing(Long productID) {
		String sql = "SELECT * FROM product_pricing WHERE in_active = 1 and product_id = " + productID;
		ProductPricing productPricing = this.jdbcTemplate.query(sql, new ResultSetExtractor<ProductPricing>() {

			@Override
			public ProductPricing extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					ProductPricing productPricing = new ProductPricing();
					
					productPricing.setId(rs.getLong("id"));
					productPricing.setProductId(rs.getLong("product_id"));
					productPricing.setBasePrice(rs.getInt("base_price"));
					productPricing.setDateCreated(rs.getDate("date_created"));
					productPricing.setDateExpiry(rs.getDate("date_expiry"));
					productPricing.setInActive(rs.getString("in_active"));
					
					return productPricing;
				}
				
				return null;
			}
			
		});
		return productPricing;
	}
}
