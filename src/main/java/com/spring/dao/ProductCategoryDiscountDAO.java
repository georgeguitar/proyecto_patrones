package com.spring.dao;

import com.spring.model.CategoryPrice;

public interface ProductCategoryDiscountDAO {
	public CategoryPrice getProductCategoryDiscount(String productID, String couponCode);
}
