package com.spring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;


import com.spring.model.ProductDiscount;
import com.spring.model.ProductPricing;

public class ProductDiscountDAOImpl implements ProductDiscountDAO {
	private JdbcTemplate jdbcTemplate;

	public ProductDiscountDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<ProductDiscount> getProductDiscounts(String productID) {
		String sql = "SELECT * FROM product_discount WHERE and product_id = " + productID;
		List<ProductDiscount> listaProductDiscount = this.jdbcTemplate.query(sql, new RowMapper<ProductDiscount>() {

			@Override
			public ProductDiscount mapRow(ResultSet rs, int rowNum) throws SQLException {
				ProductDiscount productDiscount = new ProductDiscount();
	
				productDiscount.setId(rs.getLong("id"));
				productDiscount.setProductId(rs.getLong("product_id"));
				productDiscount.setDiscountValue(rs.getInt("discount_value"));
				productDiscount.setDiscountUnit(rs.getString("discount_unit"));
				productDiscount.setDateCreated(rs.getDate("date_created"));
				productDiscount.setValidUntil(rs.getDate("valid_until"));
				productDiscount.setCouponCode(rs.getString("coupon_code"));
				productDiscount.setMinimumOrderValue(rs.getInt("minimum_order_value"));
				productDiscount.setMaximumDiscountAmount(rs.getInt("maximum_discount_amount"));
				productDiscount.setIdRedeemAllowed(rs.getString("id_redeem_allowed"));
				
				return productDiscount;
			}
		});
		
		return listaProductDiscount;
	}
	
	@Override
	public ProductDiscount getProductDiscount(String productID) {
		String sql = "SELECT * FROM product_discount WHERE id_redeem_allowed = 1 and  product_id = " + productID;
		ProductDiscount productDiscount = this.jdbcTemplate.query(sql, new ResultSetExtractor<ProductDiscount>() {

			@Override
			public ProductDiscount extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					ProductDiscount productDiscount = new ProductDiscount();
					
					productDiscount.setId(rs.getLong("id"));
					productDiscount.setProductId(rs.getLong("product_id"));
					productDiscount.setDiscountValue(rs.getInt("discount_value"));
					productDiscount.setDiscountUnit(rs.getString("discount_unit"));
					productDiscount.setDateCreated(rs.getDate("date_created"));
					productDiscount.setValidUntil(rs.getDate("valid_until"));
					productDiscount.setCouponCode(rs.getString("coupon_code"));
					productDiscount.setMinimumOrderValue(rs.getInt("minimum_order_value"));
					productDiscount.setMaximumDiscountAmount(rs.getInt("maximum_discount_amount"));
					productDiscount.setIdRedeemAllowed(rs.getString("id_redeem_allowed"));
					
					return productDiscount;
				}
				
				return null;
			}
			
		});
		return productDiscount;
	}

	
	@Override
	public ProductDiscount getProductDiscountCoupon(String productID, String couponCode) {
		String sql = "SELECT * FROM product_discount WHERE id_redeem_allowed = 1 and  product_id = " + productID + " and coupon_code = " + couponCode;
		ProductDiscount productDiscount = this.jdbcTemplate.query(sql, new ResultSetExtractor<ProductDiscount>() {

			@Override
			public ProductDiscount extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					ProductDiscount productDiscount = new ProductDiscount();
					
					productDiscount.setId(rs.getLong("id"));
					productDiscount.setProductId(rs.getLong("product_id"));
					productDiscount.setDiscountValue(rs.getInt("discount_value"));
					productDiscount.setDiscountUnit(rs.getString("discount_unit"));
					productDiscount.setDateCreated(rs.getDate("date_created"));
					productDiscount.setValidUntil(rs.getDate("valid_until"));
					productDiscount.setCouponCode(rs.getString("coupon_code"));
					productDiscount.setMinimumOrderValue(rs.getInt("minimum_order_value"));
					productDiscount.setMaximumDiscountAmount(rs.getInt("maximum_discount_amount"));
					productDiscount.setIdRedeemAllowed(rs.getString("id_redeem_allowed"));
					
					return productDiscount;
				}
				
				return null;
			}
			
		});
		return productDiscount;
	}
}
