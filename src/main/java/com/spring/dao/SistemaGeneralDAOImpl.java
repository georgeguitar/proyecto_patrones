/**
 * 
 */
package com.spring.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

public class SistemaGeneralDAOImpl implements SistemaGeneralDAO {
	private JdbcTemplate jdbcTemplate;

	public SistemaGeneralDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public Date getDateServer() {
//		String sql = "select date_format(NOW(),'%d/%m/%Y/ %h:%i:%s') as fecha";
		String sql = "select NOW() as fecha";
		Date fechaActual = this.jdbcTemplate.query(sql, new ResultSetExtractor<Date>() {

			@Override
			public Date extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					Date fechaActual = rs.getDate("fecha");
					
					return fechaActual;
				}
				
				return null;
			}
			
		});
		return fechaActual;
	}
	
	@Override
	public String getYearServer() {
		String sql = "select date_format(NOW(),'%Y') as anio";
		String fechaActual = this.jdbcTemplate.query(sql, new ResultSetExtractor<String>() {

			@Override
			public String extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					String fechaActual = rs.getString("anio");
					
					return fechaActual;
				}
				
				return null;
			}
			
		});
		return fechaActual;
	}
}
