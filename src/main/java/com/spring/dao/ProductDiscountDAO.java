package com.spring.dao;

import java.util.List;

import com.spring.model.ProductDiscount;

public interface ProductDiscountDAO {
	public List<ProductDiscount> getProductDiscounts(String productID);
	public ProductDiscount getProductDiscount(String productID);
	public ProductDiscount getProductDiscountCoupon(String productID, String couponCode);
}
