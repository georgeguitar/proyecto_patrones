package com.spring.dao;

import com.spring.model.ProductPricing;

public interface ProductPricingDAO {
	public ProductPricing getProductPricing(Long productID);
}
