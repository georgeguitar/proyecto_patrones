/**
 * 
 */
package com.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.spring.dao.ProductCategoryDAO;
import com.spring.dao.ProductCategoryDAOImpl;
import com.spring.dao.ProductCategoryDiscountDAO;
import com.spring.dao.ProductCategoryDiscountDAOImpl;
import com.spring.dao.ProductDAO;
import com.spring.dao.ProductDAOImpl;
import com.spring.dao.ProductDiscountDAO;
import com.spring.dao.ProductDiscountDAOImpl;
import com.spring.dao.ProductPricingDAO;
import com.spring.dao.ProductPricingDAOImpl;
import com.spring.dao.SistemaGeneralDAO;
import com.spring.dao.SistemaGeneralDAOImpl;


@Configuration
public class GlobalConfig  extends WebConfig {
	
	@Bean
	public SistemaGeneralDAO getSistemaGeneralDAO() {
		return new SistemaGeneralDAOImpl(getDataSource());
	}
	
	@Bean
	public ProductDAO getProductDAO() {
		return new ProductDAOImpl(getDataSource());
	}
	
	@Bean
	public ProductCategoryDAO getProductCategoryDAO() {
		return new ProductCategoryDAOImpl(getDataSource());
	}

	@Bean
	public ProductPricingDAO getProductPricingDAO() {
		return new ProductPricingDAOImpl(getDataSource());
	}

	@Bean
	public ProductDiscountDAO getProductDiscountDAO() {
		return new ProductDiscountDAOImpl(getDataSource());
	}
	
	@Bean
	public ProductCategoryDiscountDAO getProductCategoryDiscountDAO() {
		return new ProductCategoryDiscountDAOImpl(getDataSource());
	}
}
