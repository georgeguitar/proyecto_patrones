package com.spring.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.spring.dao.ProductCategoryDAO;
import com.spring.dao.ProductCategoryDiscountDAO;
import com.spring.dao.ProductDAO;
import com.spring.dao.ProductDiscountDAO;
import com.spring.dao.ProductPricingDAO;
import com.spring.dao.SistemaGeneralDAO;
import com.spring.model.CategoryPrice;
import com.spring.model.Product;
import com.spring.model.ProductCategory;
import com.spring.model.ProductDiscount;
import com.spring.model.ProductPrice;
import com.spring.model.ProductPricing;
import com.spring.patrones.estrategia.CuponCategoria;
import com.spring.patrones.estrategia.CuponProducto;
import com.spring.patrones.estrategia.DescuentoPorPorcentajeCategoria;
import com.spring.patrones.estrategia.DescuentoPorPorcentajeProduct;
import com.spring.patrones.iterador.Iterador;

@Controller
public class MainController {
	
	@Autowired
	private SistemaGeneralDAO sistemaGeneralDAO;
	
	@Autowired
	private ProductDAO productDAO;

	@Autowired
	private ProductCategoryDAO productCategoryDAO;
	
	@Autowired
	private ProductPricingDAO productPricingDAO;
	
	@Autowired
	private ProductDiscountDAO productDiscountDAO;
	
	@Autowired
	private ProductCategoryDiscountDAO productCategoryDiscountDAO;
	
   @RequestMapping(path={ "/" },method=RequestMethod.GET)
   public String inicio(Model model) {
      model.addAttribute("message","Hello Spring MVC!");
     
      model.addAttribute("year", sistemaGeneralDAO.getYearServer());
      
      return "index";
   }
   
   @RequestMapping(path={ "/menu" },method=RequestMethod.POST)
   public String menu(Model model) {
      model.addAttribute("message","Hello Spring MVC!");
     
      model.addAttribute("year", sistemaGeneralDAO.getYearServer());

      List<ProductCategory> listaProductCategorias = new ArrayList<ProductCategory>();
      listaProductCategorias = productCategoryDAO.getListaProductosCategoria();
      model.addAttribute("categorias", listaProductCategorias);
      for (ProductCategory lista : listaProductCategorias) {
    	  System.out.println(lista.getCategoryName());
      }
     
      return "menu";
   }

	@RequestMapping(value = "/productos", method = RequestMethod.GET)
	public String productos(@RequestParam String idCategoria, Model model, HttpServletRequest request, HttpServletResponse response) {
      model.addAttribute("year", sistemaGeneralDAO.getYearServer());

      List<Product> listaProductos = new ArrayList<Product>();
      listaProductos = productDAO.getProductosCategoria(idCategoria);

      //Patrón iterador.
      Iterador listaProductPrice = new Iterador();
      for (Product producto : listaProductos) {
          ProductPricing productPricing = new ProductPricing(); 
          productPricing = productPricingDAO.getProductPricing(producto.getId());
          
          ProductPrice productPrice = new ProductPrice();
          productPrice.setProduct(producto);
          productPrice.setProductPricing(productPricing);
          
          listaProductPrice.addElemento(productPrice);
    	  System.out.println(productPrice.getProduct().getProductName() + ", id: " + productPrice.getProduct().getId() + ", precio: " + productPrice.getProductPricing().getBasePrice());
      }
      model.addAttribute("listaProductPrice", listaProductPrice.getIterador());
      
      ProductCategory categoria = new ProductCategory(); 
      categoria = productCategoryDAO.getCategoria(idCategoria);
      model.addAttribute("categoria", categoria);
      
      return "productos";
   }
	
	@RequestMapping(value = "/pagar_producto",  method = {RequestMethod.POST, RequestMethod.GET})
	public String pagarProducto(@RequestParam String idProducto, Model model, HttpServletRequest request, HttpServletResponse response) {
		String mensajeRetorno = "";
		Integer cantidad = Integer.valueOf(request.getParameter("materialFormRegisterCantidad"));
		String cupon = request.getParameter("materialFormRegisterCupon");
		Date fechaActual = sistemaGeneralDAO.getDateServer();
		
		Float descuentoTotalProducto = 0.0F;
		String nombreProducto = "";
		String nombreCantidad = "unidad";
		Float descuentoTotalCategoria = 0.0F;
		Float aPagar = 0.0F;
		
		/*
		 * Debemos de sacar el nombre para mostrarlo en pantalla
		 */
		
		Product producto = new Product(); 
		producto = productDAO.getProducto(idProducto);
		nombreProducto = producto.getProductName();
		
		CategoryPrice categoryPrice = new CategoryPrice();
		categoryPrice = productCategoryDiscountDAO.getProductCategoryDiscount(idProducto, cupon);
		descuentoTotalCategoria = procesarCompraCategoria(cupon, categoryPrice, cantidad);
		
		Integer precioCategoria = categoryPrice.getProductPricing().getBasePrice();
		Integer totalPagar = cantidad * precioCategoria;
		
		/*
		 * Terminando
		 */
		
		ProductDiscount productDiscount = new ProductDiscount(); 
		productDiscount = productDiscountDAO.getProductDiscount(idProducto);
		
		if (cupon.equals(productDiscount.getCouponCode())) {
			if (fechaActual.compareTo(productDiscount.getValidUntil()) == 0 || fechaActual.compareTo(productDiscount.getValidUntil()) < 0) {
				descuentoTotalProducto = procesarCompraProducto(cupon, productDiscount, cantidad);
				if (descuentoTotalProducto > productDiscount.getMaximumDiscountAmount()) {
					descuentoTotalProducto = Float.valueOf(productDiscount.getMaximumDiscountAmount());
					mensajeRetorno = "Se llegó al máximo descuento posible, el descuento corresponde al tope máximo.";
					System.out.println(mensajeRetorno);
				} else {
					if (cantidad < productDiscount.getMinimumOrderValue()) {
						descuentoTotalProducto = 0.0F;
						mensajeRetorno = "No se pudo hacer el descuento por producto, se debe hacer una compra con una cantidad superior a: " + productDiscount.getMinimumOrderValue();
						System.out.println(mensajeRetorno);						
					} else {
						mensajeRetorno = "Cupon encontrado, id: " + productDiscount.getId() + ", cupon:" + productDiscount.getCouponCode();
						System.out.println(mensajeRetorno);					
					}
				}
			} else {
				mensajeRetorno = "Fechas no validas, cupon: " + productDiscount.getCouponCode();
				System.out.println(mensajeRetorno);
			}
		} else {
			mensajeRetorno = "Cupon no encontrado: " + cupon;
			System.out.println(mensajeRetorno);
		}		
		
		aPagar = totalPagar - descuentoTotalProducto - descuentoTotalCategoria;
		
		model.addAttribute("descuentoTotalProducto", descuentoTotalProducto);
		model.addAttribute("mensajeRetorno", mensajeRetorno);
		model.addAttribute("year", sistemaGeneralDAO.getYearServer());

		// Para el mensaje si tiene una unidad o varias
		if (cantidad >= 2) {
			nombreCantidad = "unidades";
		}
		
		model.addAttribute("precioCategoria", precioCategoria);
		model.addAttribute("aPagar", aPagar);		
		model.addAttribute("descuentoTotalCategoria", descuentoTotalCategoria);
		model.addAttribute("cantidad", cantidad);
		model.addAttribute("nombreCantidad", nombreCantidad);
		model.addAttribute("nombreProducto", nombreProducto);
		
		return "descuento";
	}
	
	private Float procesarCompraProducto(String cupon, ProductDiscount productDiscount, Integer cantidad) {
		Float descuentoTotal = 0.0F;
		
		//Patrón estrategía. Por si en el futuro se aplica otro tipo de descuento.
		CuponProducto cuponProducto = new CuponProducto();
		cuponProducto.setAlgoritmoDescuento(new DescuentoPorPorcentajeProduct(productDiscount, cantidad));
		descuentoTotal = cuponProducto.aplicarDescuento();
		
		return descuentoTotal;
	}
	
	private Float procesarCompraCategoria(String cupon, CategoryPrice categoryPrice, Integer cantidad) {
		Float descuentoTotal = 0.0F;
		
		//Patrón estrategía. Por si en el futuro se aplica otro tipo de descuento.
		CuponCategoria cuponCategoria = new CuponCategoria();
		cuponCategoria.setAlgoritmoDescuento(new DescuentoPorPorcentajeCategoria(categoryPrice, cantidad));
		descuentoTotal = cuponCategoria.aplicarDescuento();
		
		return descuentoTotal;
	}
}
