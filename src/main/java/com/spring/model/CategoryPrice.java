package com.spring.model;

/** Patron Composite **/
public class CategoryPrice {
	Product product;
	ProductPricing productPricing;
	ProductCategory productCategory;
	ProductCategoryDiscount productCategoryDiscount;
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public ProductPricing getProductPricing() {
		return productPricing;
	}
	public void setProductPricing(ProductPricing productPricing) {
		this.productPricing = productPricing;
	}
	public ProductCategory getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}
	public ProductCategoryDiscount getProductCategoryDiscount() {
		return productCategoryDiscount;
	}
	public void setProductCategoryDiscount(ProductCategoryDiscount productCategoryDiscount) {
		this.productCategoryDiscount = productCategoryDiscount;
	}
}
