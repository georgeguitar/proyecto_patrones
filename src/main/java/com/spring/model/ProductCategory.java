package com.spring.model;

public class ProductCategory {
	private Long id;
	private String categoryName;
	private Integer maxRewardPointsEncash;
	private Long parentCategoryID;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Integer getMaxRewardPointsEncash() {
		return maxRewardPointsEncash;
	}
	public void setMaxRewardPointsEncash(Integer maxRewardPointsEncash) {
		this.maxRewardPointsEncash = maxRewardPointsEncash;
	}
	public Long getParentCategoryID() {
		return parentCategoryID;
	}
	public void setParentCategoryID(Long parentCategoryID) {
		this.parentCategoryID = parentCategoryID;
	}
}
