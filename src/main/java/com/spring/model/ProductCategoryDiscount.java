package com.spring.model;

public class ProductCategoryDiscount {
	private Long id;
	private Long productCategoryId;
	private Integer discountValue;
	private String discountUnit;
	private java.sql.Date dateCreated;
	private java.sql.Date validUntil;
	private String couponCode;
	private Integer minimunOrderValue;
	private Integer maximunDiscountAmount;
	private String isRedeemAllowed;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getProductCategoryId() {
		return productCategoryId;
	}
	public void setProductCategoryId(Long productCategoryId) {
		this.productCategoryId = productCategoryId;
	}
	public Integer getDiscountValue() {
		return discountValue;
	}
	public void setDiscountValue(Integer discountValue) {
		this.discountValue = discountValue;
	}
	public String getDiscountUnit() {
		return discountUnit;
	}
	public void setDiscountUnit(String discountUnit) {
		this.discountUnit = discountUnit;
	}
	public java.sql.Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(java.sql.Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public java.sql.Date getValidUntil() {
		return validUntil;
	}
	public void setValidUntil(java.sql.Date validUntil) {
		this.validUntil = validUntil;
	}
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public Integer getMinimunOrderValue() {
		return minimunOrderValue;
	}
	public void setMinimumOrderValue(Integer minimunOrderValue) {
		this.minimunOrderValue = minimunOrderValue;
	}
	public Integer getMaximumDiscountAmount() {
		return maximunDiscountAmount;
	}
	public void setMaximumDiscountAmount(Integer maximunDiscountAmount) {
		this.maximunDiscountAmount = maximunDiscountAmount;
	}
	public String getIdRedeemAllowed() {
		return isRedeemAllowed;
	}
	public void setIdRedeemAllowed(String isRedeemAllowed) {
		this.isRedeemAllowed = isRedeemAllowed;
	}
}
