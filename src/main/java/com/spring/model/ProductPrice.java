package com.spring.model;

/** Patron Composite **/
public class ProductPrice extends ProductPricing{
	private Product product; 
	private ProductPricing productPricing;

	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public ProductPricing getProductPricing() {
		return productPricing;
	}
	public void setProductPricing(ProductPricing productPricing) {
		this.productPricing = productPricing;
	}
}
