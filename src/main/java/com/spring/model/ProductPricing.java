package com.spring.model;

public class ProductPricing {
	private Long id;
	private Long productId;
	private Integer basePrice;
	private java.sql.Date dateCreated;
	private java.sql.Date dateExpiry;
	private String inActive;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Integer getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(Integer basePrice) {
		this.basePrice = basePrice;
	}
	public java.sql.Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(java.sql.Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public java.sql.Date getDateExpiry() {
		return dateExpiry;
	}
	public void setDateExpiry(java.sql.Date dateExpiry) {
		this.dateExpiry = dateExpiry;
	}
	public String getInActive() {
		return inActive;
	}
	public void setInActive(String inActive) {
		this.inActive = inActive;
	}
}
