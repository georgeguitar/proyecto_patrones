package com.spring.model;

public class ProductDiscount {
	private Long id;
	private Long productId;
	private Integer discountValue;
	private String discountUnit;
	private java.sql.Date dateCreated;
	private java.sql.Date validUntil;
	private String couponCode;
	private Integer minimumOrderValue;
	private Integer maximumDiscountAmount;
	private String idRedeemAllowed;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Integer getDiscountValue() {
		return discountValue;
	}
	public void setDiscountValue(Integer discountValue) {
		this.discountValue = discountValue;
	}
	public String getDiscountUnit() {
		return discountUnit;
	}
	public void setDiscountUnit(String discountUnit) {
		this.discountUnit = discountUnit;
	}
	public java.sql.Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(java.sql.Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public java.sql.Date getValidUntil() {
		return validUntil;
	}
	public void setValidUntil(java.sql.Date validUntil) {
		this.validUntil = validUntil;
	}
	public String getCouponCode() {
		return couponCode;
	}
	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	public Integer getMinimumOrderValue() {
		return minimumOrderValue;
	}
	public void setMinimumOrderValue(Integer minimumOrderValue) {
		this.minimumOrderValue = minimumOrderValue;
	}
	public Integer getMaximumDiscountAmount() {
		return maximumDiscountAmount;
	}
	public void setMaximumDiscountAmount(Integer maximumDiscountAmount) {
		this.maximumDiscountAmount = maximumDiscountAmount;
	}
	public String getIdRedeemAllowed() {
		return idRedeemAllowed;
	}
	public void setIdRedeemAllowed(String idRedeemAllowed) {
		this.idRedeemAllowed = idRedeemAllowed;
	}
}
