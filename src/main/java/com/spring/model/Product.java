package com.spring.model;

public class Product {
	private Long id;
	private String productName;
	private String productDescription;
	private Integer unitsInStock;
	private Long productCategoryID;
	private Integer rewardPointsCredit;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public Integer getUnitsInStock() {
		return unitsInStock;
	}
	public void setUnitsInStock(Integer unitsInStock) {
		this.unitsInStock = unitsInStock;
	}
	public Long getProductCategoryID() {
		return productCategoryID;
	}
	public void setProductCategoryID(Long productCategoryID) {
		this.productCategoryID = productCategoryID;
	}
	public Integer getRewardPointsCredit() {
		return rewardPointsCredit;
	}
	public void setRewardPointsCredit(Integer rewardPointsCredit) {
		this.rewardPointsCredit = rewardPointsCredit;
	}
}