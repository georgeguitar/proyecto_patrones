# Módulo Patrones de Diseño

## Proyecto Final

El requerimiento es hacer una aplicación que permita generar descuentos por compra en el prototipo de una tienda de e-commerce utilizando el modelo de base de datos proporcionado y aplicando en la codificacion al menos 3 patrones de diseño de comportamiento y/o estructurales.

1. Debe permitir un descuento por porcentaje.
2. Debe permitir un descuento por periodos (fechas-horas).
3. Debe permitir hacer un descuento por caegoria de productos.
4. Cupon solo valido para una sola compra.
5. El cupon debe permitir un tope maximo por compra de productos (cantidad monetaria) o por compra minima. Ej. En la compra de $1000 de aplica en 10% de descuento.

## Forma de entrega

1. Subir el codigo a Github/GitLab.
2. Proporcionar el enlace del Github/GitLab en la entrega de proyecto del Moodle con un archivo en Word adjunto donde debera explicar que patrones utilizo y porque.

## Integrantes

Navarro Arias Juan Dirceu.  
Navarro Arias Luis Fernando Numa.  

## Herramientas

- Software de comunicación: Whatsapp, Google Docs. 
- Software de control de versiones: GitLab.
- Lenguage de desarrollo: Java
- Framework de desarrollo: Spring 5.0.4
- IDE de desarrollo: Eclipse Oxygen.2 Release (4.7.2)
- Kit de desarrolo de java (JDK): Oracle jdk-9.0.4
- Sistemas operativos: Debian 9, MS Windows 8.
- Java Servlet Container: Apache Tomcat 9.0.5
- Navegador de Internet: Debian: Firefox  52.6.0, Ms Windows Firefox 58.0.2  
- Base de datos: MariaDB 5.5.5

## Levantar el proyecto en el servidor Tomcat.

Para la base de datos:
1. Crear una base de datos de Mysql/MariaDB.
2. Configurar la conexión a la DB: /proyecto_cupones/src/resources/jdbc.properties
3. Importar el script de la DB: /proyecto_cupones/sql/Script_DB_data.sql

Para compilar se puede hacerlo desde Eclipse: 

1. Importar el proyecto descargado con git: File -> Import -> Existing Porject into Workspace -> Select root directory. 
2. Hacer un REFRESH (F5)
3. Dentro del Menu Proyect hacer click en la opción Clean (el proyecto debe estar seleccionado).
4. En el proyecto click derecho Run as > Run configurations > Maven build > Crear una nueva compilacion con las opciones: Goals clean install y tickeado Skip Tests y por ultimo hacer click en Run.
5. Crear un servidor para añadir el archivo war a Tomcat. Pestaña Servers -> Create a new server -> Elegir Tomcat 9 en la lista de Apache ->  Añadir proyecto_cupones.
6. Arrancar el servidor Tomcat desde Eclipse: click derecho en Tomcat v9.0 y elegir Start.
7. Hacer la compra con el cupon EXAMEN.
8. Visualizar en su navegador con el enlace http://localhost:8080/proyecto_cupones/


## Tips

Si se esta haciendo desde Windows es posible que al momento de compilar de un error en la base de datos mysql.

1. Añadir en el archivo de mysql config en la seccion [mysqld] default_time_zone='+03:00'.
2. Reiniciar el servidor Mysql.

